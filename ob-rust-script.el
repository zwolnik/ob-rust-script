;; Description
;; Copyright (C) Maciej Zwoliński
;;
;; Author: Maciej Zwoliński
;; Keywords: literate programming, reproducible research
;; Homepage: https://orgmode.org
;; Version: 0.01
;;
;; License:
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.
;;
;; Requirements:
;;
;; Use this section to list the requirements of this language.  Most
;; languages will require that at least the language be installed on
;; the user's system, and the Emacs major mode relevant to the
;; language be installed as well.

(require 'ob)
(require 'rust-mode)

;; Use rust mode for rust-script src blocks
(add-to-list 'org-src-lang-modes '("rust-script" . rust))

;; Use rs extension for tangling rust-script src blocks
(add-to-list 'org-babel-tangle-lang-exts '("rust-script" . "rs"))

(defvar org-babel-default-header-args:rust-script '())


;;;; Variables exporting from Lisp to Rust
;; Helper function for checking if all elements of list are equal
(defun equalp (first second)
  (equal (type-of first) (type-of second)))

(defun are-all (bin-pred cons-cell)
    (or (null (cdr cons-cell))
        (and (funcall bin-pred (car cons-cell) (cadr cons-cell))
             (are-all bin-pred (cdr cons-cell)))))
;;

(defun rust-map-value-to-rust-script (val)
  "Convert value to rust depending on it's type.
- =t= and =nil= evaluates to =true= and =false=
- =Integers= and =floats= are passed as is
- =Strings= are passed as =string slice=
- =Lists= are passed depending on their content
  - If all arguments are the same type, =array slice= is created
  - If above is not true, =tuple= is created
- Other values are passed as =string slice=."
  (cond ((stringp val) (format "\"%s\"" val))
        ((booleanp val) (if val "true" "false"))
        ((or (integerp val) (floatp val)) val)
        ((or (listp val) (arrayp val) (consp val))
         (let ((concat-cons
                (lambda (cons-cell)
                  (mapconcat (lambda (el) (format "%s" (rust-map-value-to-rust-script el)))
                             cons-cell
                             ", "))))
           (if (are-all 'equalp val)
               (concat "&[" (funcall concat-cons val) "]")
               (concat "(" (funcall concat-cons val) ")"))))
        (t (format "\"%s\"" val))))

(defun org-babel-rust-var-to-rust-script (var)
  "Convert variable binding to valid rust code"
  (format "let %s = %s;"
          (car var)
          (rust-map-value-to-rust-script (cdr var))))

;;;; Handle execution
(defun org-babel-expand-body:rust-script (body params)
  (let ((vars (org-babel--get-vars params)))
    (concat
     (mapconcat 'org-babel-rust-var-to-rust-script vars "\n") "\n"
     (if (not (member "output" (cdr (assq :result-params params))))
         (concat "println!(\"~:BEGIN:~{:?}\", {\n" body "\n});\n")
       (concat "let _ = {\n" body "\n};\n")))))

(defun org-babel-execute:rust-script (body params)
  (let ((full-body (org-babel-expand-body:rust-script body params))
        (tmp-src-file (org-babel-temp-file "rust-script-src-" ".rs")))
    (with-temp-file tmp-src-file (insert full-body))
    (let ((result (org-babel-eval
                   (format "rust-script %s --verbose" tmp-src-file) "")))
      (table-or-string (string-trim result) params))))


;;;; Process results
;; Helper functions for stdout transitions
(defun lines (text)
  (split-string text "\n"))

(defun list-of-lists (text)
  (mapcar (lambda (x) (split-string x " "))
          (lines text)))
;;

(defun is-text-table (result)
  "Check whether you can form a uniform table from result"
  (or (null (cdr (lines result)))
      (are-all (lambda (x y) (equal (length x) (length y)))
               (list-of-lists result))))

(defun get-result-value (result)
  "Return result after ~:BEGIN:~ marker"
  (with-temp-buffer
    (progn (insert result)
           (goto-char (point-min))
           (search-forward "~:BEGIN:~")
           (buffer-substring-no-properties (point) (point-max)))))

(defun table-or-string (result params)
  "Return string or table based on context"
  ;; Maybe extract this
  (if (member "output" (cdr (assq :result-params params)))
      result
    (let ((result-value (get-result-value result)))
      (if (is-text-table result-value)
          (org-babel-reassemble-table
           (org-babel-script-escape (org-trim result-value))
           (org-babel-pick-name (cdr (assq :colname-names params))
                                (cdr (assq :colnames params)))
           (org-babel-pick-name (cdr (assq :rowname-names params))
                                (cdr (assq :rownames params))))
        (org-babel-script-escape (org-trim result-value))))))


;;;; Sessions (not supported)
(defun org-babel-prep-session:rust-script (_session _params)
  "This function does nothing as rust is compiled language"
  (error "Rust is a compiled language -- no support for sessions"))

(defun org-babel-rust-script-initiate-session (&optional _session)
  "This function does nothing as rust is compiled language"
  (error "Rust is a compiled language -- no support for sessions"))

(provide 'ob-rust-script)
;;; ob-rust-script.el ends here
