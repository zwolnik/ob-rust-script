#+TITLE: Ob Rust script
#+AUTHOR: Maciej Zwoliński
#+DATE: Sun Jun 27 18:59:49 2021

* Description
  Copyright (C) Maciej Zwoliński

  Author: Maciej Zwoliński
  Keywords: literate programming, reproducible research
  Homepage: https://orgmode.org
  Version: 0.01

** License:

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with GNU Emacs; see the file COPYING.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

** Requirements:

   Use this section to list the requirements of this language.  Most
   languages will require that at least the language be installed on
   the user's system, and the Emacs major mode relevant to the
   language be installed as well.

* Bash -> Rust -> Python
  #+name: bash-l
  #+begin_src bash
    cd ~ && du -s * | grep -v total | head -n 5
  #+end_src

  #+RESULTS: bash-l
  |  6352 | Audio  |
  | 11880 | backup |
  | 32216 | builds |
  |    16 | certs  |
  |     4 | data   |

  #+name: rust-l
  #+begin_src rust-script :var x=bash-l
    x.iter()
        .map(|(size, name)| (name, size))
        .collect::<Vec<_>>()
  #+end_src

  #+RESULTS: rust-l
  | Audio  |  6352 |
  | backup | 11880 |
  | builds | 32216 |
  | certs  |    16 |
  | data   |     4 |

  #+begin_src python :var x=rust-l
    return max([int(y) for x, y in x])
  #+end_src

  #+RESULTS:
  : 32216

* Regular inputs
** Results variants
*** Single int
    :PROPERTIES:
    :header-args: :var x=10
    :END:
**** Default results
***** Rust
****** Stdout
       #+name: SingleIntDefaultRustStdout
       #+begin_src rust-script
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleIntDefaultRustStdout

****** Return
       #+name: SingleIntDefaultRustReturn
       #+begin_src rust-script
         x
       #+end_src

       #+RESULTS: SingleIntDefaultRustReturn
       : 10

***** Common Lisp
****** Stdout
       #+name: SingleIntDefaultCLispStdout
       #+begin_src lisp
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleIntDefaultCLispStdout
       : NIL

****** Return
       #+name: SingleIntDefaultCLispReturn
       #+begin_src lisp
         x
       #+end_src

       #+RESULTS: SingleIntDefaultCLispReturn
       : 10

***** Emacs Lisp
****** Stdout
       #+name: SingleIntDefaultELispStdout
       #+begin_src elisp
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleIntDefaultELispStdout

****** Return
       #+name: SingleIntDefaultELispReturn
       #+begin_src elisp
         x
       #+end_src

       #+RESULTS: SingleIntDefaultELispReturn
       : 10

***** Python
****** Stdout
       #+name: SingleIntDefaultPythonStdout
       #+begin_src python
         print(x)
       #+end_src

       #+RESULTS: SingleIntDefaultPythonStdout
       : None

****** Return
       #+name: SingleIntDefaultPythonReturn
       #+begin_src python
         return x
       #+end_src

       #+RESULTS: SingleIntDefaultPythonReturn
       : 10

**** Results Output
***** Rust
****** Stdout
       #+name: SingleIntOutputRustStdout
       #+begin_src rust-script :results output
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleIntOutputRustStdout
       : 10

****** Return
       #+name: SingleIntOutputRustReturn
       #+begin_src rust-script :results output
         x
       #+end_src

       #+RESULTS: SingleIntOutputRustReturn

***** Common Lisp
****** Stdout
       #+name: SingleIntOutputCLispStdout
       #+begin_src lisp :results output
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleIntOutputCLispStdout
       : 10

****** Return
       #+name: SingleIntOutputCLispReturn
       #+begin_src lisp :results output
         x
       #+end_src

       #+RESULTS: SingleIntOutputCLispReturn

***** Emacs Lisp
****** Stdout
       #+name: SingleIntOutputELispStdout
       #+begin_src elisp :results output
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleIntOutputELispStdout
       : 10

****** Return
       #+name: SingleIntOutputELispReturn
       #+begin_src elisp :var x=1 :results output
         x
       #+end_src

       #+RESULTS: SingleIntOutputELispReturn

***** Python
****** Stdout
       #+name: SingleIntOutputPythonStdout
       #+begin_src python :results output
         print(x)
       #+end_src

       #+RESULTS: SingleIntOutputPythonStdout
       : 10

****** Return
       #+name: SingleIntOutputPythonReturn
       #+begin_src python :results output
         return x
       #+end_src

       #+RESULTS: SingleIntOutputPythonReturn

**** Results value
***** Rust
****** Stdout
       #+name: SingleIntValueRustStdout
       #+begin_src rust-script :results value
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleIntValueRustStdout

       #+RESULTS:
       | 10 |

****** Return
       #+name: SingleIntValueRustReturn
       #+begin_src rust-script :results value
         x
       #+end_src

       #+RESULTS: SingleIntValueRustReturn
       : 10

***** Common Lisp
****** Stdout
       #+name: SingleIntValueCLispStdout
       #+begin_src lisp :results value
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleIntValueCLispStdout
       : NIL

       #+RESULTS:
       : NIL

****** Return
       #+name: SingleIntValueCLispReturn
       #+begin_src lisp :results value
         x
       #+end_src

       #+RESULTS: SingleIntValueCLispReturn
       : 10

       #+RESULTS:
       : 10

***** Emacs Lisp
****** Stdout
       #+name: SingleIntValueELispStdout
       #+begin_src elisp :results value
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleIntValueELispStdout

****** Return
       #+name: SingleIntValueELispReturn
       #+begin_src elisp :results value
         x
       #+end_src

       #+RESULTS: SingleIntValueELispReturn
       : 10

***** Python
****** Stdout
       #+name: SingleIntValuePythonStdout
       #+begin_src python :results value
         print(x)
       #+end_src

       #+RESULTS: SingleIntValuePythonStdout
       : None

****** Return
       #+name: SingleIntValuePythonReturn
       #+begin_src python :results value
         return x
       #+end_src

       #+RESULTS: SingleIntValuePythonReturn
       : 10

**** Comparison table
     |            | Default |        | Output |        | Value  |        |
     |            | Stdout  | Return | Stdout | Return | Stdout | Return |
     |------------+---------+--------+--------+--------+--------+--------|
     | Rust       | nil     |     10 |     10 |        | nil    |     10 |
     | CommonLisp | NIL     |     10 |     10 |        | NIL    |     10 |
     | EmacsLisp  | nil     |     10 |     10 |        | nil    |     10 |
     | Python     | None    |     10 |     10 | nil    | None   |     10 |
     #+TBLFM: @3$2='(org-sbe SingleIntDefaultRustStdout)::@3$3='(org-sbe SingleIntDefaultRustReturn)::@3$4='(org-sbe SingleIntOutputRustStdout)::@3$5='(org-sbe SingleIntOutputRustReturn)::@3$6='(org-sbe SingleIntValueRustStdout)::@3$7='(org-sbe SingleIntValueRustReturn)::@4$2='(org-sbe SingleIntDefaultCLispStdout)::@4$3='(org-sbe SingleIntDefaultCLispReturn)::@4$4='(org-sbe SingleIntOutputCLispStdout)::@4$5='(org-sbe SingleIntOutputCLispReturn)::@4$6='(org-sbe SingleIntValueCLispStdout)::@4$7='(org-sbe SingleIntValueCLispReturn)::@5$2='(org-sbe SingleIntDefaultELispStdout)::@5$3='(org-sbe SingleIntDefaultELispReturn)::@5$4='(org-sbe SingleIntOutputELispStdout)::@5$5='(org-sbe SingleIntOutputELispReturn)::@5$6='(org-sbe SingleIntValueELispStdout)::@5$7='(org-sbe SingleIntValueELispReturn)::@6$2='(org-sbe SingleIntDefaultPythonStdout)::@6$3='(org-sbe SingleIntDefaultPythonReturn)::@6$4='(org-sbe SingleIntOutputPythonStdout)::@6$5='(org-sbe SingleIntOutputPythonReturn)::@6$6='(org-sbe SingleIntValuePythonStdout)::@6$7='(org-sbe SingleIntValuePythonReturn)

*** Single string
    :PROPERTIES:
    :header-args: :var x="foo"
    :END:
**** Default results
***** Rust
****** Stdout
       #+name: SingleStringDefaultRustStdout
       #+begin_src rust-script
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleStringDefaultRustStdout

****** Return
       #+name: SingleStringDefaultRustReturn
       #+begin_src rust-script
         x
       #+end_src

       #+RESULTS: SingleStringDefaultRustReturn
       : foo

***** Common Lisp
****** Stdout
       #+name: SingleStringDefaultCLispStdout
       #+begin_src lisp
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleStringDefaultCLispStdout
       : NIL

****** Return
       #+name: SingleStringDefaultCLispReturn
       #+begin_src lisp
         x
       #+end_src

       #+RESULTS: SingleStringDefaultCLispReturn
       : foo

***** Emacs Lisp
****** Stdout
       #+name: SingleStringDefaultELispStdout
       #+begin_src elisp
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleStringDefaultELispStdout

****** Return
       #+name: SingleStringDefaultELispReturn
       #+begin_src elisp
         x
       #+end_src

       #+RESULTS: SingleStringDefaultELispReturn
       : foo

***** Python
****** Stdout
       #+name: SingleStringDefaultPythonStdout
       #+begin_src python
         print(x)
       #+end_src

       #+RESULTS: SingleStringDefaultPythonStdout
       : None

****** Return
       #+name: SingleStringDefaultPythonReturn
       #+begin_src python
         return x
       #+end_src

       #+RESULTS: SingleStringDefaultPythonReturn
       : foo

**** Results Output
***** Rust
****** Stdout
       #+name: SingleStringOutputRustStdout
       #+begin_src rust-script :results output
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleStringOutputRustStdout
       : foo

****** Return
       #+name: SingleStringOutputRustReturn
       #+begin_src rust-script :results output
         x
       #+end_src

       #+RESULTS: SingleStringOutputRustReturn

***** Common Lisp
****** Stdout
       #+name: SingleStringOutputCLispStdout
       #+begin_src lisp :results output
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleStringOutputCLispStdout
       : foo

****** Return
       #+name: SingleStringOutputCLispReturn
       #+begin_src lisp :results output
         x
       #+end_src

       #+RESULTS: SingleStringOutputCLispReturn

***** Emacs Lisp
****** Stdout
       #+name: SingleStringOutputELispStdout
       #+begin_src elisp :results output
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleStringOutputELispStdout
       : foo

****** Return
       #+name: SingleStringOutputELispReturn
       #+begin_src elisp :var x=1 :results output
         x
       #+end_src

       #+RESULTS: SingleStringOutputELispReturn

***** Python
****** Stdout
       #+name: SingleStringOutputPythonStdout
       #+begin_src python :results output
         print(x)
       #+end_src

       #+RESULTS: SingleStringOutputPythonStdout
       : foo

****** Return
       #+name: SingleStringOutputPythonReturn
       #+begin_src python :results output
         return x
       #+end_src

       #+RESULTS: SingleStringOutputPythonReturn

**** Results value
***** Rust
****** Stdout
       #+name: SingleStringValueRustStdout
       #+begin_src rust-script :results value
         println!("{}", x);
       #+end_src

       #+RESULTS: SingleStringValueRustStdout

       #+RESULTS:
       | 10 |

****** Return
       #+name: SingleStringValueRustReturn
       #+begin_src rust-script :results value
         x
       #+end_src

       #+RESULTS: SingleStringValueRustReturn
       : foo

***** Common Lisp
****** Stdout
       #+name: SingleStringValueCLispStdout
       #+begin_src lisp :results value
         (format t "~a" x)
       #+end_src

       #+RESULTS: SingleStringValueCLispStdout
       : NIL

       #+RESULTS:
       : NIL

****** Return
       #+name: SingleStringValueCLispReturn
       #+begin_src lisp :results value
         x
       #+end_src

       #+RESULTS: SingleStringValueCLispReturn
       : foo

       #+RESULTS:
       : 10

***** Emacs Lisp
****** Stdout
       #+name: SingleStringValueELispStdout
       #+begin_src elisp :results value
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: SingleStringValueELispStdout

****** Return
       #+name: SingleStringValueELispReturn
       #+begin_src elisp :results value
         x
       #+end_src

       #+RESULTS: SingleStringValueELispReturn
       : foo

***** Python
****** Stdout
       #+name: SingleStringValuePythonStdout
       #+begin_src python :results value
         print(x)
       #+end_src

       #+RESULTS: SingleStringValuePythonStdout
       : None

****** Return
       #+name: SingleStringValuePythonReturn
       #+begin_src python :results value
         return x
       #+end_src

       #+RESULTS: SingleStringValuePythonReturn
       : foo

**** Comparison table
     |            | Default |        | Output |        | Value  |        |
     |            | Stdout  | Return | Stdout | Return | Stdout | Return |
     |------------+---------+--------+--------+--------+--------+--------|
     | Rust       | nil     | foo    | foo    |        | nil    | foo    |
     | CommonLisp | NIL     | foo    | foo    |        | NIL    | foo    |
     | EmacsLisp  | nil     | foo    | foo    |        | nil    | foo    |
     | Python     | None    | foo    | foo    | nil    | None   | foo    |
     #+TBLFM: @3$2='(org-sbe SingleStringDefaultRustStdout)::@3$3='(org-sbe SingleStringDefaultRustReturn)::@3$4='(org-sbe SingleStringOutputRustStdout)::@3$5='(org-sbe SingleStringOutputRustReturn)::@3$6='(org-sbe SingleStringValueRustStdout)::@3$7='(org-sbe SingleStringValueRustReturn)::@4$2='(org-sbe SingleStringDefaultCLispStdout)::@4$3='(org-sbe SingleStringDefaultCLispReturn)::@4$4='(org-sbe SingleStringOutputCLispStdout)::@4$5='(org-sbe SingleStringOutputCLispReturn)::@4$6='(org-sbe SingleStringValueCLispStdout)::@4$7='(org-sbe SingleStringValueCLispReturn)::@5$2='(org-sbe SingleStringDefaultELispStdout)::@5$3='(org-sbe SingleStringDefaultELispReturn)::@5$4='(org-sbe SingleStringOutputELispStdout)::@5$5='(org-sbe SingleStringOutputELispReturn)::@5$6='(org-sbe SingleStringValueELispStdout)::@5$7='(org-sbe SingleStringValueELispReturn)::@6$2='(org-sbe SingleStringDefaultPythonStdout)::@6$3='(org-sbe SingleStringDefaultPythonReturn)::@6$4='(org-sbe SingleStringOutputPythonStdout)::@6$5='(org-sbe SingleStringOutputPythonReturn)::@6$6='(org-sbe SingleStringValuePythonStdout)::@6$7='(org-sbe SingleStringValuePythonReturn)

*** List
    :PROPERTIES:
    :header-args: :var x='(10 15)
    :END:
**** Default results
***** Rust
****** Stdout
       #+name: ListDefaultRustStdout
       #+begin_src rust-script
         println!("{:?}", x);
       #+end_src

       #+RESULTS: ListDefaultRustStdout

****** Return
       #+name: ListDefaultRustReturn
       #+begin_src rust-script
         x
       #+end_src

       #+RESULTS: ListDefaultRustReturn
       | 10 | 15 |

***** Common Lisp
****** Stdout
       #+name: ListDefaultCLispStdout
       #+begin_src lisp
         (format t "~a" x)
       #+end_src

       #+RESULTS: ListDefaultCLispStdout
       : NIL

****** Return
       #+name: ListDefaultCLispReturn
       #+begin_src lisp
         x
       #+end_src

       #+RESULTS: ListDefaultCLispReturn
       | 10 | 15 |

***** Emacs Lisp
****** Stdout
       #+name: ListDefaultELispStdout
       #+begin_src elisp
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: ListDefaultELispStdout

****** Return
       #+name: ListDefaultELispReturn
       #+begin_src elisp
         x
       #+end_src

       #+RESULTS: ListDefaultELispReturn
       | 10 | 15 |

***** Python
****** Stdout
       #+name: ListDefaultPythonStdout
       #+begin_src python
         print(x)
       #+end_src

       #+RESULTS: ListDefaultPythonStdout
       : None

****** Return
       #+name: ListDefaultPythonReturn
       #+begin_src python
         return x
       #+end_src

       #+RESULTS: ListDefaultPythonReturn
       | 10 | 15 |

**** Results Output
***** Rust
****** Stdout
       #+name: ListOutputRustStdout
       #+begin_src rust-script :results output
         println!("{:?}", x);
       #+end_src

       #+RESULTS: ListOutputRustStdout
       : [10, 15]

****** Return
       #+name: ListOutputRustReturn
       #+begin_src rust-script :results output
         x
       #+end_src

       #+RESULTS: ListOutputRustReturn

***** Common Lisp
****** Stdout
       #+name: ListOutputCLispStdout
       #+begin_src lisp :results output
         (format t "~a" x)
       #+end_src

       #+RESULTS: ListOutputCLispStdout
       : (10 15)

****** Return
       #+name: ListOutputCLispReturn
       #+begin_src lisp :results output
         x
       #+end_src

       #+RESULTS: ListOutputCLispReturn

***** Emacs Lisp
****** Stdout
       #+name: ListOutputELispStdout
       #+begin_src elisp :results output
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: ListOutputELispStdout
       : (10 15)

****** Return
       #+name: ListOutputELispReturn
       #+begin_src elisp :var x=1 :results output
         x
       #+end_src

       #+RESULTS: ListOutputELispReturn

***** Python
****** Stdout
       #+name: ListOutputPythonStdout
       #+begin_src python :results output
         print(x)
       #+end_src

       #+RESULTS: ListOutputPythonStdout
       : [10, 15]

****** Return
       #+name: ListOutputPythonReturn
       #+begin_src python :results output
         return x
       #+end_src

       #+RESULTS: ListOutputPythonReturn

**** Results value
***** Rust
****** Stdout
       #+name: ListValueRustStdout
       #+begin_src rust-script :results value
         println!("{:?}", x);
       #+end_src

       #+RESULTS: ListValueRustStdout

       #+RESULTS:
       | 10 |

****** Return
       #+name: ListValueRustReturn
       #+begin_src rust-script :results value
         x
       #+end_src

       #+RESULTS: ListValueRustReturn
       | 10 | 15 |

***** Common Lisp
****** Stdout
       #+name: ListValueCLispStdout
       #+begin_src lisp :results value
         (format t "~a" x)
       #+end_src

       #+RESULTS: ListValueCLispStdout
       : NIL

       #+RESULTS:
       : NIL

****** Return
       #+name: ListValueCLispReturn
       #+begin_src lisp :results value
         x
       #+end_src

       #+RESULTS: ListValueCLispReturn
       | 10 | 15 |

       #+RESULTS:
       : 10

***** Emacs Lisp
****** Stdout
       #+name: ListValueELispStdout
       #+begin_src elisp :results value
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: ListValueELispStdout

****** Return
       #+name: ListValueELispReturn
       #+begin_src elisp :results value
         x
       #+end_src

       #+RESULTS: ListValueELispReturn
       | 10 | 15 |

***** Python
****** Stdout
       #+name: ListValuePythonStdout
       #+begin_src python :results value
         print(x)
       #+end_src

       #+RESULTS: ListValuePythonStdout
       : None

****** Return
       #+name: ListValuePythonReturn
       #+begin_src python :results value
         return x
       #+end_src

       #+RESULTS: ListValuePythonReturn
       | 10 | 15 |

**** Comparison table
     |            | Default |         | Output   |        | Value  |         |
     |            | Stdout  | Return  | Stdout   | Return | Stdout | Return  |
     |------------+---------+---------+----------+--------+--------+---------|
     | Rust       | nil     | (10 15) | [10, 15] |        | nil    | (10 15) |
     | CommonLisp | NIL     | (10 15) | (10 15)  |        | NIL    | (10 15) |
     | EmacsLisp  | nil     | (10 15) | (10 15)  |        | nil    | (10 15) |
     | Python     | None    | (10 15) | [10, 15] | nil    | None   | (10 15) |
     #+TBLFM: @3$2='(org-sbe ListDefaultRustStdout)::@3$3='(org-sbe ListDefaultRustReturn)::@3$4='(org-sbe ListOutputRustStdout)::@3$5='(org-sbe ListOutputRustReturn)::@3$6='(org-sbe ListValueRustStdout)::@3$7='(org-sbe ListValueRustReturn)::@4$2='(org-sbe ListDefaultCLispStdout)::@4$3='(org-sbe ListDefaultCLispReturn)::@4$4='(org-sbe ListOutputCLispStdout)::@4$5='(org-sbe ListOutputCLispReturn)::@4$6='(org-sbe ListValueCLispStdout)::@4$7='(org-sbe ListValueCLispReturn)::@5$2='(org-sbe ListDefaultELispStdout)::@5$3='(org-sbe ListDefaultELispReturn)::@5$4='(org-sbe ListOutputELispStdout)::@5$5='(org-sbe ListOutputELispReturn)::@5$6='(org-sbe ListValueELispStdout)::@5$7='(org-sbe ListValueELispReturn)::@6$2='(org-sbe ListDefaultPythonStdout)::@6$3='(org-sbe ListDefaultPythonReturn)::@6$4='(org-sbe ListOutputPythonStdout)::@6$5='(org-sbe ListOutputPythonReturn)::@6$6='(org-sbe ListValuePythonStdout)::@6$7='(org-sbe ListValuePythonReturn)

*** Nested List
    :PROPERTIES:
    :header-args: :var x='((10 "foo") (12 "bar"))
    :END:
**** Default results
***** Rust
****** Stdout
       #+name: NestedListDefaultRustStdout
       #+begin_src rust-script
         println!("{:?}", x);
       #+end_src

       #+RESULTS: NestedListDefaultRustStdout

****** Return
       #+name: NestedListDefaultRustReturn
       #+begin_src rust-script
         x
       #+end_src

       #+RESULTS: NestedListDefaultRustReturn
       | 10 | foo |
       | 12 | bar |

***** Common Lisp
****** Stdout
       #+name: NestedListDefaultCLispStdout
       #+begin_src lisp
         (format t "~a" x)
       #+end_src

       #+RESULTS: NestedListDefaultCLispStdout
       : NIL

****** Return
       #+name: NestedListDefaultCLispReturn
       #+begin_src lisp
         x
       #+end_src

       #+RESULTS: NestedListDefaultCLispReturn
       | 10 | foo |
       | 12 | bar |

***** Emacs Lisp
****** Stdout
       #+name: NestedListDefaultELispStdout
       #+begin_src elisp
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: NestedListDefaultELispStdout

****** Return
       #+name: NestedListDefaultELispReturn
       #+begin_src elisp
         x
       #+end_src

       #+RESULTS: NestedListDefaultELispReturn
       | 10 | foo |
       | 12 | bar |

***** Python
****** Stdout
       #+name: NestedListDefaultPythonStdout
       #+begin_src python
         print(x)
       #+end_src

       #+RESULTS: NestedListDefaultPythonStdout
       : None

****** Return
       #+name: NestedListDefaultPythonReturn
       #+begin_src python
         return x
       #+end_src

       #+RESULTS: NestedListDefaultPythonReturn
       | 10 | foo |
       | 12 | bar |

**** Results Output
***** Rust
****** Stdout
       #+name: NestedListOutputRustStdout
       #+begin_src rust-script :results output
         println!("{:?}", x);
       #+end_src

       #+RESULTS: NestedListOutputRustStdout
       : [(10, "foo"), (12, "bar")]

****** Return
       #+name: NestedListOutputRustReturn
       #+begin_src rust-script :results output
         x
       #+end_src

       #+RESULTS: NestedListOutputRustReturn

***** Common Lisp
****** Stdout
       #+name: NestedListOutputCLispStdout
       #+begin_src lisp :results output
         (format t "~a" x)
       #+end_src

       #+RESULTS: NestedListOutputCLispStdout
       : ((10 foo) (12 bar))

****** Return
       #+name: NestedListOutputCLispReturn
       #+begin_src lisp :results output
         x
       #+end_src

       #+RESULTS: NestedListOutputCLispReturn

***** Emacs Lisp
****** Stdout
       #+name: NestedListOutputELispStdout
       #+begin_src elisp :results output
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: NestedListOutputELispStdout
       : ((10 foo) (12 bar))

****** Return
       #+name: NestedListOutputELispReturn
       #+begin_src elisp :var x=1 :results output
         x
       #+end_src

       #+RESULTS: NestedListOutputELispReturn

***** Python
****** Stdout
       #+name: NestedListOutputPythonStdout
       #+begin_src python :results output
         print(x)
       #+end_src

       #+RESULTS: NestedListOutputPythonStdout
       : [[10, 'foo'], [12, 'bar']]

****** Return
       #+name: NestedListOutputPythonReturn
       #+begin_src python :results output
         return x
       #+end_src

       #+RESULTS: NestedListOutputPythonReturn

**** Results value
***** Rust
****** Stdout
       #+name: NestedListValueRustStdout
       #+begin_src rust-script :results value
         println!("{:?}", x);
       #+end_src

       #+RESULTS: NestedListValueRustStdout

****** Return
       #+name: NestedListValueRustReturn
       #+begin_src rust-script :results value
         x
       #+end_src

       #+RESULTS: NestedListValueRustReturn
       | 10 | foo |
       | 12 | bar |

***** Common Lisp
****** Stdout
       #+name: NestedListValueCLispStdout
       #+begin_src lisp :results value
         (format t "~a" x)
       #+end_src

       #+RESULTS: NestedListValueCLispStdout
       : NIL

       #+RESULTS:
       : NIL

****** Return
       #+name: NestedListValueCLispReturn
       #+begin_src lisp :results value
         x
       #+end_src

       #+RESULTS: NestedListValueCLispReturn
       | 10 | foo |
       | 12 | bar |

       #+RESULTS:
       : 10

***** Emacs Lisp
****** Stdout
       #+name: NestedListValueELispStdout
       #+begin_src elisp :results value
         (progn (princ x)
                nil)
       #+end_src

       #+RESULTS: NestedListValueELispStdout

****** Return
       #+name: NestedListValueELispReturn
       #+begin_src elisp :results value
         x
       #+end_src

       #+RESULTS: NestedListValueELispReturn
       | 10 | foo |
       | 12 | bar |

***** Python
****** Stdout
       #+name: NestedListValuePythonStdout
       #+begin_src python :results value
         print(x)
       #+end_src

       #+RESULTS: NestedListValuePythonStdout
       : None

****** Return
       #+name: NestedListValuePythonReturn
       #+begin_src python :results value
         return x
       #+end_src

       #+RESULTS: NestedListValuePythonReturn
       | 10 | foo |
       | 12 | bar |

**** Comparison table
     |            | Default |                         | Output                     |        | Value  |                         |
     |            | Stdout  | Return                  | Stdout                     | Return | Stdout | Return                  |
     |------------+---------+-------------------------+----------------------------+--------+--------+-------------------------|
     | Rust       | nil     | ((10 "foo") (12 "bar")) | [(10, "foo"), (12, "bar")] |        | nil    | ((10 "foo") (12 "bar")) |
     | CommonLisp | NIL     | ((10 "foo") (12 "bar")) | ((10 foo) (12 bar))        |        | NIL    | ((10 "foo") (12 "bar")) |
     | EmacsLisp  | nil     | ((10 "foo") (12 "bar")) | ((10 foo) (12 bar))        |        | nil    | ((10 "foo") (12 "bar")) |
     | Python     | None    | ((10 "foo") (12 "bar")) | [[10, 'foo'], [12, 'bar']] | nil    | None   | ((10 "foo") (12 "bar")) |
     #+TBLFM: @3$2='(org-sbe NestedListDefaultRustStdout)::@3$3='(org-sbe NestedListDefaultRustReturn)::@3$4='(org-sbe NestedListOutputRustStdout)::@3$5='(org-sbe NestedListOutputRustReturn)::@3$6='(org-sbe NestedListValueRustStdout)::@3$7='(org-sbe NestedListValueRustReturn)::@4$2='(org-sbe NestedListDefaultCLispStdout)::@4$3='(org-sbe NestedListDefaultCLispReturn)::@4$4='(org-sbe NestedListOutputCLispStdout)::@4$5='(org-sbe NestedListOutputCLispReturn)::@4$6='(org-sbe NestedListValueCLispStdout)::@4$7='(org-sbe NestedListValueCLispReturn)::@5$2='(org-sbe NestedListDefaultELispStdout)::@5$3='(org-sbe NestedListDefaultELispReturn)::@5$4='(org-sbe NestedListOutputELispStdout)::@5$5='(org-sbe NestedListOutputELispReturn)::@5$6='(org-sbe NestedListValueELispStdout)::@5$7='(org-sbe NestedListValueELispReturn)::@6$2='(org-sbe NestedListDefaultPythonStdout)::@6$3='(org-sbe NestedListDefaultPythonReturn)::@6$4='(org-sbe NestedListOutputPythonStdout)::@6$5='(org-sbe NestedListOutputPythonReturn)::@6$6='(org-sbe NestedListValuePythonStdout)::@6$7='(org-sbe NestedListValuePythonReturn)

* Value mode
** Main function
   #+begin_src rust-script
     fn main() {
         3 + 2
     }
   #+end_src

** Function definition
   #+begin_src rust-script
     fn fib(count: u32) -> u32 {
         match count {
             0 => 0,
             1 => 1,
             2 => 1,
             _ => fib(count - 1) + fib(count - 2)
         }
     }

     (1..10).map(fib).collect::<Vec<_>>()
   #+end_src

   #+RESULTS:
   | 1 | 1 | 2 | 3 | 5 | 8 | 13 | 21 | 34 |

** Struct definition and return
   #+begin_src rust-script

   #+end_src

   #+RESULTS:
   : A { foo: 12 }

   #+begin_src rust-script
     let x: Result<(), _> = Err("abc");
     x.unwrap()
   #+end_src

* Table inputs
** Regular table
   #+name: files-to-process
   | num | file       | size | owner   |
   |-----+------------+------+---------|
   |   1 | foo.txt    | 10kB | me      |
   |   2 | bar        | 1MB  | someone |
   |   3 | baz        | 2b   | me      |
   |   4 | readme.org | 345b | me      |

   #+begin_src rust-script :var files=files-to-process :rownames yes
     files
         .iter()
         .filter(|[_, _, owner]| owner != &"me")
         .collect::<Vec<_>>()
   #+end_src

   #+RESULTS:
   | bar | 1MB | someone |

   #+begin_src elisp :var files=files-to-process :rownames yes
     (seq-filter (lambda (row)
                   (not (string= (car (last row)) "me")))
                 files)
   #+end_src

   #+RESULTS:
   | bar | 1MB | someone |

   #+BEGIN_SRC python :var files=files-to-process :rownames yes
     return [(name, size, owner) for (name, size, owner) in files if owner != "me"]
   #+END_SRC

   #+RESULTS:
   | bar | 1MB | someone |

** Sparse table
   #+name: files-to-process-missing
   | num | file       | size | owner   |
   |-----+------------+------+---------|
   |   1 | foo.txt    | 10kB | me      |
   |   2 | bar        |      | someone |
   |   3 |            | 2b   | me      |
   |   4 | readme.org | 345b | me      |

   #+begin_src rust-script :var files=files-to-process-missing :rownames yes
     files
         .iter()
         .filter(|[_, _, owner]| owner != &"me")
         .for_each(|[file, size, owner]| println!("{} {} {}", file, size, owner))
   #+end_src

   #+RESULTS:
   | bar |   | someone |

   #+begin_src elisp :var files=files-to-process-missing :rownames yes
     (seq-filter (lambda (row)
                   (not (string= (car (last row)) "me")))
                 files)
   #+end_src

   #+RESULTS:
   | bar |   | someone |

   #+BEGIN_SRC python :var files=files-to-process-missing :rownames yes
     return [(name, size, owner) for (name, size, owner) in files if owner != "me"]
   #+END_SRC

   #+RESULTS:
   | bar |   | someone |
